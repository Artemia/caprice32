// Caprice32 Image Disk Selector
// Inherited from CFrame

#ifndef _WG_CAPRICE32OPENDISK_H_
#define _WG_CAPRICE32OPENDISK_H_

#include "wgui.h"
#include "wg_frame.h"
#include "wg_label.h"
#include "cap32.h"

namespace wGui
{
    class CapriceOpenDisk : public CFrame {
      public:
        //! \param pParent A pointer to the parent view
        //! \param pFontEngine A pointer to the font engine to use when drawing the control
        //! If this is set to 0 it will use the default font engine specified by the CApplication (which must be set before instantiating this object)
        // selectedRomButton is the button that was clicked to open this dialog (not the nicest solution, but it works...)
        CapriceOpenDisk(const CRect& WindowRect, CWindow* pParent, CFontEngine* pFontEngine, std::string sTitle, int selectedDrive, t_drive* Drive);
        bool HandleMessage(CMessage* pMessage);
        // Returns a list with the available ROM files (filenames)
        std::vector<std::string> getAvailableDisk();

      protected:

        int selDrive;  // selected ROM slot number
        t_drive* pDrive;
        std::string DrivePath;
        std::string DriveFile;

        CButton* m_pButtonOpen;  // Inserts the selected ROM in the ROM slot.
        CButton* m_pButtonClear;   // Clears the selected ROM slot.
        CButton* m_pButtonCancel;  // Close the dialog without action.

        CListBox* m_pListBoxDisk;  // Lists the available ROM files (in the ROMS subdirectory)

      private:
        void operator=(CapriceOpenDisk) { }  //!< The assignment operator is not allowed for CWindow derived objects
    };
}

#endif  // _WG_CAPRICE32ROMSLOTS_H_
