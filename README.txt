Caprice32 - Amstrad CPC Emulator
http://github.com/Neophile76/Caprice32

Fork by Neophile from the original emulator:

(c) Copyright 1997-2004 Ulrich Doewich available at http://sourceforge.net/projects/caprice32/



..........................................................................

WHAT IS IT?
~~~~~~~~~~~

Caprice32 is a software emulator of the Amstrad CPC 8bit home computer
series. The emulator faithfully imitates the CPC464, CPC664, and CPC6128
models. By recreating the operations of all hardware components at a low
level, the emulator achieves a high degree of compatibility with original
CPC software. These programs or games can be run unmodified at real-time
or higher speeds, depending on the emulator host environment.


..........................................................................

USING THE SOURCE
~~~~~~~~~~~~~~~~

The source for Caprice32 is distributed under the terms of the GNU
General Public License (GNU GPL), which is included in this archive as
COPYING.txt. Please make sure that you understand the terms and
conditions of the license before using the source.

I encourage you to get involved in the project - please see the Caprice32
pages on SourceForge.net for contact details.


..........................................................................

REQUIREMENTS
~~~~~~~~~~~~

You will need the following to successfully compile an executable:

Code::Blocks - http://www.codeblocks.org/
MinGW (only for Windows) - http://sourceforge.net/projects/mingw/
or TDM-GCC - http://tdm-gcc.tdragon.net/
SDL 1.2 - http://www.libsdl.org/index.php
zLib - http://www.gzip.org/zlib/
freetype - https://www.freetype.org/


..........................................................................

COMPILING
~~~~~~~~~

Windows target:

Use CodeBlocks IDE with caprice32.cbp

VC Studio is not yet available for this fork

Linux target:

Use CodeBlocks IDE with caprice32.cbp

Note for sound with pulse audio: 
If you encouter strange sound with crackle, put in your profile this export :

export SDL_AUDIODRIVER=alsa

The sound should be nice after that.

..........................................................................

COMMENTS OR READY TO CONTRIBUTE?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have suggestions, a bug report or even want to join the
development team, please feel free to use one of the many contact methods
presented on the Caprice32 fork project page on github.com - see the URL
at the top.
