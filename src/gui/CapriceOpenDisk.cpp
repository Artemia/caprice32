// Caprice32 ROM slot selection window
// Inherited from CFrame

#include <dirent.h>
#include "CapriceOpenDisk.h"
#include "cap32.h"
#include "fileutils.h"

// CPC emulation properties, defined in cap32.h:
extern t_CPC CPC;

namespace wGui {

CapriceOpenDisk::CapriceOpenDisk(const CRect& WindowRect, CWindow* pParent, CFontEngine* pFontEngine, std::string sTitle, int selectedDrive, t_drive* Drive) :
	CFrame(WindowRect, pParent, pFontEngine, sTitle, false)
{
	pDrive=Drive;
    selDrive = selectedDrive;
    if (selDrive)
	 {
	 	DrivePath = CPC.drvB_path;
	 	DriveFile = CPC.drvB_file;
	 	SetWindowText("Drive B Loader ");
	 } else
	 {
	 	DrivePath = CPC.drvA_path;
	 	DriveFile = CPC.drvA_file;
	 	SetWindowText("Drive A Loader");
	 }

	m_pButtonOpen = new CButton(CRect(CPoint( 40, m_ClientRect.Height() - 22), 50, 15), this, "Open");
	m_pButtonClear  = new CButton(CRect(CPoint(100, m_ClientRect.Height() - 22), 50, 15), this, "Clear");
	m_pButtonCancel = new CButton(CRect(CPoint(160, m_ClientRect.Height() - 22), 50, 15), this, "Cancel");

	m_pListBoxDisk = new CListBox(CRect(CPoint(10, 10), m_ClientRect.Width() - 25, 140), this, true);

	std::vector<std::string> diskFiles = getAvailableDisk();
	for (unsigned int i = 0; i < diskFiles.size(); i ++)
	{
		m_pListBoxDisk->AddItem(SListItem(diskFiles.at(i)));
		if (diskFiles.at(i) == DriveFile) { // It's all based on the filename of the ROM,
                                                                    // maybe find a better way.
			m_pListBoxDisk->SetSelection(i, true);
			m_pListBoxDisk->SetFocus(i);
		}
	}
}

bool CapriceOpenDisk::HandleMessage(CMessage* pMessage)
{
	bool bHandled = false;
	if (pMessage)
	{
		switch(pMessage->MessageType())
        {
		case CMessage::CTRL_SINGLELCLICK:
		{
			if (pMessage->Destination() == this)
			{
				if (pMessage->Source() == m_pButtonCancel) {
					CloseFrame();
					CMessageServer::Instance().QueueMessage(new CMessage(CMessage::APP_EXIT, 0, this));
                    break;
                }
				if (pMessage->Source() == m_pButtonOpen) {
                    // Put selected ROM filename on button: (if there is a selection)
                    int selectedDiskIndex = m_pListBoxDisk->getFirstSelectedIndex();
                    if (selectedDiskIndex >= 0)
							{
                        DriveFile = (m_pListBoxDisk->GetItem(selectedDiskIndex)).sItemText;
                        if (selDrive) strncpy(CPC.drvB_file, DriveFile.c_str(), sizeof(CPC.drvB_file));
								else strncpy(CPC.drvA_file, DriveFile.c_str(), sizeof(CPC.drvA_file));
								std::string pchFile = DrivePath+DriveFile;
                        dsk_load(pchFile.c_str(),pDrive,'A');
								CloseFrame();
                        CMessageServer::Instance().QueueMessage(new CMessage(CMessage::APP_EXIT, 0, this));
                    }
                    break;
                }
				if (pMessage->Source() == m_pButtonClear)
				{
					if (selDrive)
					{
						strcpy(CPC.drvB_file, "");
					}
					else
					{
						strcpy(CPC.drvA_file, "");
					}
					dsk_eject(pDrive);
					CloseFrame();
					bHandled = true;
                    break;
                }
           		CMessageServer::Instance().QueueMessage(new CMessage(CMessage::APP_EXIT, 0, this));
			}
			break;
		}

		default :
			bHandled = CFrame::HandleMessage(pMessage);
			break;
		}
	}
	return bHandled;
}

// Reads the existing ROM filenames (in roms subdirectory defined in cap32.cfg)
std::vector<std::string> CapriceOpenDisk::getAvailableDisk() {
   // CPC.rom_path contains the ROM path, e.g. ./rom:
   return listDirectory(DrivePath);
}


}
